# Simplified DES

An S-DES implementation for a school project

## Build

```
# Build
git clone https://gitlab.com/VassilisAsteriou/sdes.git
mkdir sdes/build
cd sdes/build
cmake ../
make
# Run the tests
./sdes-test/sdes-test
```

The sdes executable is `build/sdes/sdes`

## Run:

#### Examples

1. `$ cat file.sensitive | sdes 2> secret.key > secret.txt`

Encrypt `file.sensitive` with a random key, store the key
in `secret.key` and the encrypted text in `secret.txt`

2. `$ sdes 2> secret.key --input file.sensitive --output secret.txt`

Same as above.

3. `$ sdes --input secret.txt --decrypt --key $(cat secret.key)`

Decrypts message from 1. and prints it on stdout

4. `$ cat secret.txt | sdes -D -K $(cat secret.key)`

Same as above.

#### Options

```
$ ./sdes --help

    Options:
    --key | -K <10 bits>    e.g. -K 1110001110
    --encrypt | -E          Default. Key is optional. If unspecified
                            a random key will be used that will be
                            printed on stderr.
    --decrypt | -D          Decrypt the input. A key must be provided.
    --input | -i            Input file. Default: stdin.
    --output | -o           Output file. Default: stdout.
    
```