#include "runner.h"

#include <iostream>

std::optional<test_error_t> test_t::attempt(unsigned remaining_attempts) {
    while (remaining_attempts > 0) {
        remaining_attempts--;
        try {
            run();
            std::cout << ".";
            std::cout.flush();
            return {};
        }
        catch (std::exception &e) {
            if (remaining_attempts == 0) {
                std::cout << "F";
                std::cout.flush();
                return test_error_t {
                        e.what(),
                        "",
                        name
                };
            }
        }
    }
    return {};
}

std::ostream &operator<<(std::ostream &out, test_error_t const &e) {
    out << "While testing " << e.test_set_name << ", in test case " << e.test_name << ":" << std::endl;
    out << e.error_message << std::endl;
    return out;
}
