#ifndef UNIT_TEST_REQUIRE_H
#define UNIT_TEST_REQUIRE_H

#include <string>
#include <sstream>
#include <iomanip>
#include <functional>
#include <future>
#include <cmath>

void require(bool expr, const std::string& msg = "Failed requirement");

template<class A, class B>
void requireEqual(A a, B b) {
    require(a == b, "Failed equality requirement");
}

template <class float_t>
void requireEqualFloat(float_t a, float_t b, unsigned ulp) {
    std::stringstream s;
    s << "Failed floating point equality requirement" << std::endl;
    s << std::fixed << std::setprecision(17) <<
        "a = " << a << std::endl <<
        "b = " << b << std::endl;
    const auto eps = std::numeric_limits<float_t>::epsilon() * std::abs(a+b) * std::pow(2.0, ulp);
    require(std::abs(b-a) <= eps
            || std::abs(b-a) < std::numeric_limits<float_t>::min(), s.str());
}

template<>
void requireEqual<double, double>(double a, double b);

template <>
void requireEqual<std::string, std::string>(std::string a, std::string b);

template <>
void requireEqual<std::string, const char *>(std::string a, const char *b);

template<class exception_type>
void requireThrows(std::function<void(void)> const &f)
{
    try {
        f();
        require(false, "Failed exception throw requirement");
    }
    catch (exception_type &e) {
        require(true, "Failed exception throw requirement");
    }
}

template<class exception_type>
void requireDoesntThrow(std::function<void(void)> const &f)
{
    try {
        f();
        require(true, "Failed exception not-throw requirement");
    }
    catch (exception_type &e) {
        require(false, "Failed exception not-throw requirement");
    }
}

template <>
void requireEqual<unsigned, unsigned>(unsigned, unsigned);

template <class runnable_type, class ...args_type>
auto on_different_thread(runnable_type runnable, args_type ...args)
{
    return std::async(runnable, args...).get();
}

#endif //UNIT_TEST_REQUIRE_H
