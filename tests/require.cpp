#include "require.h"

void require(bool expr, const std::string& msg) {
    if (!expr)
        throw std::runtime_error(msg);
}

template<>
void requireEqual<double, double>(double a, double b) {
    requireEqualFloat(a, b, 5);
}

template<>
void requireEqual<unsigned int, unsigned int>(unsigned a, unsigned b) {
    std::stringstream msg;
    msg << "Unsigned-unsigned comparison failed." << std::endl;
    msg << "Expected " << a << " but got " << b << " instead." << std::endl;
    require(a == b, msg.str());
}

template<>
void requireEqual<std::string, std::string>(
        std::string a,
        std::string b)
{
    std::stringstream msg;
    msg << "String comparison failed." << std::endl;
    msg << "Expected: " << a << std::endl;
    msg << "But instead got: " << b << std::endl;
    require(a == b, msg.str());
}

template<>
void requireEqual<std::string, const char *>(std::string a, const char *b) {
    requireEqual<std::string, std::string>(a,b);
}
