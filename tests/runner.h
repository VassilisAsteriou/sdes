#ifndef UNIT_TEST_RUNNER_H
#define UNIT_TEST_RUNNER_H

#include <string>
#include <functional>
#include <iostream>

struct test_error_t;

struct test_t {
    std::string name;
    std::function<void()> run;

    std::optional<test_error_t> attempt(unsigned remaining_attempts);
};

struct test_set_t {
    std::string name;
    std::vector<test_t> tests;
};

struct test_error_t {
    std::string error_message;
    std::string test_set_name;
    std::string test_name;
};

std::ostream &operator <<(std::ostream &out, test_error_t const&);

template <class test_suite_t>
void run_all_tests(test_suite_t test_suite)
{
    std::vector<test_error_t> errors;
    for (auto &test_set_generator : test_suite) {
        auto test_set = test_set_generator();
        for (auto &test: test_set.tests) {
            auto error_opt = test.attempt(5);
            if(error_opt) {
                auto error = *error_opt;
                error.test_set_name = test_set.name;
                errors.push_back(error);
            }
        }
    }
    std::cout << std::endl;
    for(auto &error_message : errors) {
        std::cout << error_message << std::endl;
    }
}

#endif //UNIT_TEST_RUNNER_H
