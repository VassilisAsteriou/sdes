
#include <require.h>
#include "sdes_test.h"

#include <bitset>
#include <calculator.h>

test_set_t sdes_test() {
    using namespace sdes;
    using std::bitset;
    return { "S-DES Helpers", {
            {"Rotate Left", [](){
                bitset<8> byte{"11001100"};
                requireEqual(rotate_left(byte).to_string(), "10011001");
                requireEqual(rotate_left(byte).to_string(), "00110011");
            }},
            {"Permutations",[](){
                bitset<2> a{"01"};

                requireEqual(a[1], false);
                requireEqual(a[0], true);
                requireEqual(permute(a, {2,1}).to_string(), "10");

                bitset<8> b{"11110000"};
                requireEqual(permute(b, {5,6,7,8,1,2,3,4}).to_string(),
                        "00001111");
            }},
            {"Concatenate", [](){
                auto x = concatenate(bitset<6>{"111000"}, bitset<4>("1010"));
                requireEqual(x.to_string(), "1110001010");
            }},
            {"Keygen", [](){
                // Παράδειγμα από τις σημειώσεις
                bitset<10> K{"1010000010"};
                keygen kg(K);

                requireEqual(kg.K1().to_string(), "10100100");
                requireEqual(kg.K2().to_string(), "01000011");
            }},
            {"that decrypting encrypted data yields the original byte", [](){
                using std::numeric_limits;
                for (unsigned k = 0; k < 5; k++) {
                    calculator C;
                    for (int i = numeric_limits<char>::min(); i <= numeric_limits<char>::max(); ++i) {
                        char c = static_cast<char>(i);
                        requireEqual(c, C.decrypt(C.encrypt(c)));
                    }
                }
            }},
            {"that example results are OK", [](){
                using std::vector;
                using std::pair;
                calculator C(bitset<10>{"1110001110"});
                // output - input pairs
                vector<pair<char, char>> examples = {
                        {0b10011100, 0b11110000},
                        {0b11001010, 0b10101010},
                        {0b11001010, 0b10101010},
                        {0b11001011, 0b00111010}
                };

                for (auto &x: examples) {
                    requireEqual(x.first, C.encrypt(x.second));
                }
            }}
        }
    };
}
