#include <runner.h>
#include "sdes_test.h"

int main() {
    auto test_suites = {
        sdes_test
    };

    run_all_tests(test_suites);
}