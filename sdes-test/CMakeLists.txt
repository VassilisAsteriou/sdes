add_executable(sdes-test
        main.cpp

        sdes_test.cpp
        sdes_test.h)

target_link_libraries(sdes-test unit-test sdes-lib)

target_include_directories(sdes-test PUBLIC ./)
