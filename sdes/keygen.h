#ifndef SDES_KEYGEN_H
#define SDES_KEYGEN_H

#include "util.h"

namespace sdes {

    /**
     * Κλάση που υπολογίζει τα κλειδιά K1 και K2
     * του αλγορίθμου.
     */
    class keygen {
        public:

            /**
             * Αρχικοποίηση.
             * Υπολογισμός των Κ1 και Κ2
             *
             * Χρησιμοποιώντας το κλειδί των 10bits υπολογίζει
             * τα K1 και Κ2, όπως ορίζονται στις σημειώσεις.
             * Με χρήση των μεταθέσεων P8, P10 και της
             * συνάρτησης shift, η υλοποίηση προκύπτει
             * άμεσα από τις σημειώσεις.
             */
            explicit keygen(bitset<10> const &key) {
                _k1 = P8(shift(P10(key)));
                _k2 = P8(shift(shift(shift(P10(key)))));
            }

            /**
             * Επιστρέφει το προϋπολογισμένο Κ1
             */
            [[nodiscard]]
            bitset<8> K1() const {
                return _k1;
            }

            /**
             * Επιστρέφει το προϋπολογισμένο Κ2
             */
            [[nodiscard]]
            bitset<8> K2() const {
                return _k2;
            }

        private:

            /**
             * Ακολουθούν οι μεταθέσεις. Ορίζονται ως στατικές
             * μεταβλητές με τύπο λ (συναρτησιακό) όπως επιστρέφεται
             * από την make_permutation.
             */
            static constexpr auto P10 = make_permutation<10,10>({
                3,5,2,7,4,10,1,9,8,6
            });

            /**
             * Επιλογή 8 από 10 bits και μετάθεση.
             */
            static constexpr auto P8 = make_permutation<8,10>({
                6,3,7,4,8,5,10,9
            });

            /**
             * Συνάρτηση που υλοποιεί την εξής λειτουργία:
             * Λαμβάνει 10 bits και εκτελεί κυκλική ολίσθηση
             * χωριστά για τα πρώτα 5 και για τα τελευταία
             * 10 bits.
             * @param k
             * @return
             */
            static bitset<10> shift(bitset<10> const &k) {
                bitset<5> five_first = k.to_ulong() >> 5u;
                bitset<5> five_last = k.to_ulong();

                rotate_left(five_first);
                rotate_left(five_last);

                return concatenate(five_first, five_last);
            }

        private:

            bitset<8> _k1;
            bitset<8> _k2;
    };

}

#endif //SDES_KEYGEB_H
