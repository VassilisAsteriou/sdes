/**
 * Βασικές λειτουργίες που χρησιμοποιούνται σε όλο το
 * πρόγραμμα:
 *  1. Παραγωγή τυχαίου κλειδιού
 *  2. Αριστερή κυκλική ολίσθηση
 *  3. Γενική μετάθεση
 *  4. Συνένωση
 */
#ifndef SDES_UTIL_H
#define SDES_UTIL_H

#include <random>
#include <bitset>
#include <vector>

namespace sdes {

    using std::random_device;
    using std::bitset;
    using std::vector;

    /**
     * Δημιουργία τυχαίου κλειδιού 10bit. Υλοποίηση μέσω λ
     * ώστε η συνάρτηση να ενθυλακώνει κατάσταση τύπου
     * random_device. Στο gcc/linux και στο msvc++ το random_device
     * είναι μη ντετερμινιστική γεννήτρια τυχαίων αριθμών.
     * Παράγει ακέραιους με ομοιόμορφη κατανομή στο [0, 2^32).
     * Κρατώντας 10 bits παίρνουμε ομοιόμορφα κατανεμημένους
     * ακέραιους στο [0, 2^10)
     */
    auto new_random_key = [rd = random_device()]() mutable {
        return bitset<10>{rd()};
    };

    /**
     * Υλοποίηση του rotate left (κυκλική ολίσθηση)
     * για bitsets. Η αρίθμηση των bits στο bitset
     * γίνεται από τα δεξιά προς τα αριστερά και
     * ξεκινά από το 0.
     */
    auto rotate_left = [](auto &_bitset) -> auto& {
        auto N = _bitset.size();
        bool temp = _bitset[N - 1];
        _bitset <<= 1u;
        _bitset.set(0, temp);
        return _bitset;
    };

    /**
     * Γενική συνάρτηση που υπολογίζει μεταθέσεις.
     * Σε ένα bitset<8> τα bits απαριθμούνται ως:
     *     76543210
     * Αλλά στις σημειώσεις που περιγράφεται ο
     * αλγόριθμος ως:
     *     12345678
     *
     * Η συνάρτηση αυτή λαμβάνει μία μετάθεση με
     * τη δεύτερη μορφή και την εφαρμόζει σε ένα
     * bitset.
     *
     * Η έξοδος μπορεί να είναι μεγαλύτερη ή
     * μικρότερη από την είσοδο (οπότε μπορεί
     * να κάνει και αντιγραφή ή επιλογή bits)
     *
     * @tparam N Πλήθος bits εισόδου
     * @tparam M Πλήθος bits εξόδου
     * @param in bitset εισόδου (N bits)
     * @param p Μετάθεση
     * @return bitset εξόδου (M bits)
     */
    template<unsigned long N, unsigned long M = N>
    bitset<N> permute(
            const bitset<M> &in,
            const unsigned (&p)[N]
    ) {
        bitset<N> out;
        for (unsigned i = 0; i < N; ++i) {
            out.set(N - i - 1, in[M - p[i]]);
        }
        return out;
    }

    /**
     * Επιστρέφει συναρτησιακά που υπολογίζουν
     * μεταθέσεις. Χρησιμοποιώντας λ, δένει την
     * μετάθεση p ενώ αφήνει ελεύθερη τη μεταβλητή
     * in της συνάρτησης permute(in, p) (παραπάνω).
     */
    template<unsigned N, unsigned M = N>
    constexpr auto make_permutation(const unsigned(&p)[N]) {
        return [=](const bitset<M> &block) {
            return permute<N, M>(block, p);
        };
    }

    /**
     * Δεδομένων δύο bitset επιστρέφει τη συνένωσή τους
     * (π.χ. για a = 111000 και b = 1010
     * δίνει 1110001010).
     *
     * @tparam N Το μήκος του πρώτου bitset
     * @tparam M Το μήκος του δεύτερου bitset
     * @param a Το πρώτο bitset
     * @param b Το δεύτερο bitset
     * @return Η συνένωση των a και b.
     */
    template <unsigned long N, unsigned long M>
    constexpr auto concatenate(bitset<N> const &a, bitset<M> const &b)
    {
        return bitset<N+M>{
            a.to_ullong() << M |
            b.to_ullong()
        };
    }

}

#endif //SDES_UTIL_H
