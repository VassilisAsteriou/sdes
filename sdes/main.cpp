/**
 * Το αρχείο αυτό έχει την υλοποίηση του command line
 * interface για το sdes. Συγκεκριμένα υλοποιούνται τα
 * command line switches:
 *     --key (no key => random if encrypt, error if decrypt)
 *     --encrypt/--decrypt (default=encrypt)
 *     --input (if unspecified then stdin)
 *     --output (if unspecified then stdout)
 *
 * Παραδείγματα:
 * 1)  $ cat file.sensitive | sdes 2> secret.key > secret.txt
 *      Κρυπτογραφεί το file.sensitive με τυχαίο κλειδί,
 *      το οποίο αποθηκεύεται στο secret.key ενώ το κρυπτογρα-
 *      φημένο κείμενο στο secret.txt
 * 2)  $ sdes 2> secret.key --input file.sensitive --output secret.txt
 *      Ταυτόσημο με το (1)
 * 3)  $ sdes --input secret.txt --decrypt --key $(cat secret.key)
 *      Αποκρυπτογραφεί το κείμενο του παραδείγματος (1) και
 *      το εκτυπώνει στην οθόνη
 * 4)  $ cat secret.txt | sdes -D -K $(cat secret.key)
 *      Το ίδιο με το παραπάνω
 */
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <sstream>

#include "calculator.h"

/**
 * Τύπος που συνοψίζει τα ορίσματα γραμμής εντολών.
 */
struct args_t {
    bool encrypt = true;
    std::bitset<10> key = sdes::new_random_key();
    std::istream *input = &std::cin;
    std::ostream *output = &std::cout;

    ~args_t() {
        if (input != &std::cin)
            delete input;

        if (output != &std::cout)
            delete output;
    }
};

/**
 * Βοηθητικός τύπος για την αποφυγή π.χ.
 * διπλών --key ή --input/--output
 */
struct encountered_t {
    bool encrypt = false;
    bool decrypt = false;
    bool key = false;
    bool input = false;
    bool output = false;

};

void app(const args_t &args);

/**
 * Συνάρτηση που επιστρέφει το επόμενο όρισμα από
 * τη λίστα argv, και ενημερώνει τον μετρητή argc
 * και τον pointer argv ώστε σε κάθε κλήση να
 * επιστρέφει το επόμενο όρισμα.
 * @param argc Πλήθων ορισμάτων που απομένουν
 * @param argv Ορίσματα που απομένουν (c-style array
 *              of c-style strings)
 * @return Το επόμενο όρισμα ως std::string
 */
std::string get_next (int &argc, char **&argv) {
    if (argc > 0) {
        std::string rv = argv[0];
        argc--; argv++;
        return rv;
    }
    else {
        throw std::runtime_error("Expected more args");
    }
}

/**
 * Τυπώνει το μήνυμα βοήθειας.
 */
void help() {
    using std::cerr;
    using std::endl;

    cerr << endl;
    cerr << "    Options:" << endl;
    cerr << "    --key | -K <10 bits>    e.g. -K 1110001110" << endl;
    cerr << "    --encrypt | -E          Default. Key is optional. If unspecified" << endl;
    cerr << "                            a random key will be used that will be" << endl;
    cerr << "                            printed on stderr." << endl;
    cerr << "    --decrypt | -D          Decrypt the input. A key must be provided." << endl;
    cerr << "    --input | -i            Input file. Default: stdin." << endl;
    cerr << "    --output | -o           Output file. Default: stdout." << endl;
    cerr << endl;
}

/**
 * Επεξεργασία ορισμάτων γραμμής εντολών.
 * @param argc
 * @param argv
 * @return
 */
args_t process_args(int argc, char *argv[])
{
    args_t args{};
    encountered_t encountered;
    bool double_arg = false;

    get_next(argc, argv); // Ignore executable name

    // Επεξεργασία switches
    while (argc > 0) {
        auto _switch = get_next(argc, argv);
        if (_switch == "--key" || _switch == "-K") {
            // Ανίχνευση διπλών switches
            if (encountered.key) double_arg = true;

            // Ενημέρωση της τιμής που θα επιστραφεί
            args.key = std::bitset<10>{get_next(argc, argv)};
            encountered.key = true;
        }
        else if (_switch == "--encrypt" || _switch == "-E") {
            if (encountered.encrypt) double_arg = true;

            args.encrypt = true;
            encountered.encrypt = true;
        }
        else if (_switch == "--decrypt" || _switch == "-D") {
            if (encountered.decrypt) double_arg = true;

            args.encrypt = false;
            encountered.decrypt = true;
        }
        else if (_switch == "--input" || _switch == "-i") {
            if (encountered.input) double_arg = true;
            else {
                // Εδώ έχει else για να αποφύγουμε leak
                args.input = new std::ifstream(get_next(argc, argv));
                encountered.input = true;
            }
        }
        else if (_switch == "--output" || _switch == "-o") {
            if (encountered.output) double_arg = true;
            else {
                args.output = new std::ofstream(get_next(argc, argv));
                encountered.output = true;
            }
        }
        else if (_switch == "--help" || _switch == "-h") {
            help();
            exit(0);
        }
        else {
            std::stringstream ss;
            ss << "Unxepected argument " << _switch << std::endl;
            throw std::runtime_error(ss.str());
        }
    }

    // Αποκλειστικά είτε encrypt είτε decrypt
    if (encountered.encrypt && encountered.decrypt) {
        throw std::runtime_error("Both --encrypt and --decrypt were given.");
    }
    // Κάθε switch μπορεί να οριστεί μέχρι μία φορά
    if (double_arg) {
        throw std::runtime_error("An argument may be only specified once.");
    }
    // Το key είναι υποχρεωτικό στο decrypt
    if (!args.encrypt && !encountered.key) {
        throw std::runtime_error("Cannot decrypt without the key!");
    }

    // Εάν το κλειδί δεν ορίζεται τυπώνει το randomly generated κλειδί
    if (!encountered.key) {
        std::cerr << args.key << std::endl;
    }

    return args;
}

/**
 * Συνάρτηση που λαμβάνει ένα συναρτησιακό τύπου
 * F: char -> char, το εφαρμόζει σε κάθε χαρακτήρα
 * ενός input stream και γράφει το αποτέλεσμα σε
 * ένα output stream.
 *
 * @tparam F Ο τύπος του συναρτησιακού
 * @param in Το input stream
 * @param out Το output stream
 * @param f Το συναρτησιακό
 */
template <class F>
void for_each_char(std::istream &in, std::ostream &out, F f)
{
    while (true) {
        char c;
        in.read(&c, 1);
        if (in.eof()) break;
        c = f(c);
        out.write(&c, 1);
    }
}

/**
 * Υλοποίηση της λογικής στο υψηλότερο επίπεδο του
 * προγράμματος. Λαμβάνει τα επεξεργασμένα ορίσματα
 * και εκτελεί την for_each_char, με συναρτησιακό
 * που κάνει είτε encrypt είτε decrypt στα bytes του
 * input.
 * Το input, το output, το εάν γίνεται encrypt ή
 * decrypt καθώς και το κλειδί δίνονται από το args
 * struct.
 * @param args Τα επεξεργασμένα ορίσματα του προγράμματος
 */
void app(const args_t &args) {
    if (args.encrypt) {
        auto encrypt = [
                calc = sdes::calculator(args.key)
        ] (char c) -> char
        {
            return calc.encrypt(c);
        };
        for_each_char(*args.input, *args.output, encrypt);
    }
    else {
        auto decrypt = [
                calc = sdes::calculator(args.key)
        ] (char c) -> char
        {
            return calc.decrypt(c);
        };
        for_each_char(*args.input, *args.output, decrypt);
    }
}

/**
 * Σημείο εισόδου του προγράμματος
 * Έλεγχος σφαλμάτων.
 */
int main(int argc, char *argv[]) {

    try {
        auto args = process_args(argc, argv);

        app(args);

        return 0;
    } catch (std::runtime_error &e) {
        using std::cerr;
        using std::endl;

        std::string exe_name{argv[0]};

        cerr << "ERROR: " << e.what() << endl;
        cerr << endl;
        cerr << "Usage: " << exe_name << " [options]" << endl;

        help();

        return 1;
    }

}
