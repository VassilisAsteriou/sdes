#ifndef SDES_FK_H
#define SDES_FK_H

#include "util.h"

namespace sdes {

    /**
     * Η κλάση f είναι υλοποίηση της συνάρτησης
     * fK ως συναρτησιακό. Με τον προσδιορισμό
     * του κλειδιού K των 8bits (είτε το K1 είτε
     * το K2) είναι εφικτός ο υπολογισμός του fK(L,R)
     * για οποιδήποτε byte <LR>.
     */
    class f {
        public:
            /**
             * Αρχικοποίηση κατάστασης συναρτησιακού
             * με το κλειδί K των 8 bit.
             */
            explicit f(bitset<8> const &K) :
                    _K{K}
            {}

            /**
             * Υπολογισμός του fK(L,R). Η συνάρτηση
             * αυτή πρώτα χωρίζει την είσοδο των 8bits στα πρώτα
             * 4 και στα τελευταία 4 bits, έπειτα υπολογίζει
             * το L xor F(R), όπου F συνάρτηση που εξαρτάται
             * και από το κλειδί K, και τέλος συναρμολογεί και
             * δίνει στην έξοδο το <L xor F(R), R>.
             */
            bitset<8> operator() (bitset<8> const &LR) const
            {
                bitset<4> L = LR.to_ulong() >> 4u;
                bitset<4> R = LR.to_ulong();

                return concatenate(L^F(R), R);
            }

        private:

            /**
             * Η συνάρτηση αυτή λειτουργεί ως εξής:
             * Δεδομένου ενός τμήματος R των 4 bits,
             * πρώτα υπολογίζει το E/P (extend and permute),
             * των 8 bits το οποίο προσθέτει με xor στο κλειδί K.
             *
             * Στη συνέχεια επεξεργάζεται με τους πίνακες
             * αντικατάστασης S0 και S1 τα πρώτα 4 και
             * τα τελευταία 4 bits χωριστά, για να λάβει
             * 2 + 2bits. Αυτά τα συναρμολογεί και επιστρέφει
             * μία μετάθεσή τους.
             */
            [[nodiscard]]
            bitset<4> F(bitset<4> R) const
            {
                auto p = EP(R) ^ _K;

                auto first_two = S(p.to_ulong() >> 4u, S0);
                auto last_two = S(p.to_ulong(), S1);

                auto combined = concatenate(first_two, last_two);

                return permute(combined, {2,4,3,1});
            }

            /**
             * Τύπος που περιγράφει μήτρα από
             * τμήματα 2 bits.
             */
            using S_t = vector<vector<bitset<2>>>;

            /**
             * Υλοποίηση των S-boxes. Χρησιμοποιεί
             * τα bits του p για να συνθέσει δύο
             * δείκτες των 2 bits τους οποίους στη συνέχεια
             * χρησιμοποιεί για να εκτελέσει μία πρόσβαση
             * στον πίνακα αντικατάστασης S.
             */
            static bitset<2> S(bitset<4> p, S_t const &S)
            {
                auto i = permute(p, {1,4}).to_ulong();
                auto j = permute(p, {2,3}).to_ulong();
                return S[i][j];
            }

            /**
             * Επέκταση και μετάθεση των 4 σε 8 bits.
             */
            static constexpr auto EP = make_permutation<8,4>({
                4,1,2,3,2,3,4,1
            });

            /**
             * Πίνακες αντικατάστασης S0 και S1, όπως
             * ορίζονται στις σημειώσεις.
             */
            const S_t S0 = {
                    {1,0,3,2},
                    {3,2,1,0},
                    {0,2,1,3},
                    {3,1,3,2}
            };

            const S_t S1 = {
                    {0,1,2,3},
                    {2,0,1,3},
                    {3,0,1,0},
                    {2,1,0,3}
            };

            bitset<8> _K;
    };

}

#endif //SDES_FK_H
