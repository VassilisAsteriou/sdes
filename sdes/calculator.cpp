
#include "calculator.h"

namespace sdes {

    calculator::calculator() :
            calculator{new_random_key()}
    {}

    calculator::calculator(bitset<10> key) :
            fK1{keygen(key).K1()},
            fK2{keygen(key).K2()},
            _key{key}
    {}

    char calculator::encrypt(char block) const {
        ByteChar _block, rv;
        _block.character = block;
        rv.uchar = combo(_block.uchar, fK1, fK2).to_ulong();
        return rv.character;
    }

    char calculator::decrypt(char block) const {
        ByteChar _block, rv;
        _block.character = block;
        rv.uchar = combo(_block.uchar, fK2, fK1).to_ulong();
        return rv.character;
    }

    bitset<8> calculator::combo(
            bitset<8> const &block,
            f const &f1,
            f const &f2) const {
        return IP_inv(f2(SW(f1(IP(block)))));
    }

}
