#ifndef SDES_CALCULATOR_H
#define SDES_CALCULATOR_H

#include "keygen.h"
#include "fK.h"

namespace sdes {

    /**
     * Κλάση που υλοποιεί την κρυπτογράφηση
     * και αποκρυπτογράφηση με τη χρήση SDES.
     */
    class calculator {
        public:
            /**
             * Αρχικοποίηση με τυχαίο κλειδί 10 bits.
             */
            calculator();

            /**
             * Αρχικοποίηση με δεδομένο κλειδί 10 bits.
             * Υπολογίζει τα K1 και K2 και αρχικοποιεί
             * τις συναρτήσεις fK1 και fK2 που
             * χρησιμοποιούνται στην κρυπτογράφηση και την
             * αποκρυπτογράφηση.
             */
            explicit calculator(bitset<10> key);

            [[nodiscard]]
            inline bitset<10> const &key() const {
                return _key;
            }

            /**
             * Η συνάρτηση αυτή λαμβάνει ένα char που παριστάνει
             * ένα block μήκους 8 bits, το μετατρέπει σε bitset
             * υπολογίζει το αποτέλεσμα της κρυπτογράφησης
             * του, και το επιστρέφει αφού το ξαναμετατρέψει σε
             * char.
             *
             * Με την παρατήρηση της συμμετρίας των συναρτήσεων
             * κρυπτογράφησης και αποκρυπτογράφησης, τα κοινά
             * τους σημεία απομονώθηκαν στη συνάρτηση combo.
             */
            [[nodiscard]]
            char encrypt(char block) const;

            /**
             * Όμοια με τη συνάρτηση κρυπτογράφηση, μόνο που
             * αλλάζουν οι ρόλοι των fK1 και fK2.
             */
            [[nodiscard]]
            char decrypt(char block) const;

        private:

            /**
             * Τύπος που χρησιμοποιείται στη
             * μετατροπή του char.
             */
            union ByteChar {
                char character;
                unsigned char uchar;

                ByteChar() : character{0} {}
            };

            /**
             * Υλοποίηση της σύνθεσης συναρτήσεων που υλοποιεί
             * την κρυπτογράφηση και την αποκρυπτογράφηση.
             */
            [[nodiscard]]
            bitset<8> combo(
                    bitset<8> const &block,
                    f const &f1, f const &f2
            ) const;

            /**
             * Ορισμοί μεταθέσεων initial permutation,
             * όπως δίνονται στις σημειώσεις.
             */
            static constexpr auto IP = make_permutation<8, 8>({
                2, 6, 3, 1, 4, 8, 5, 7
            });

            static constexpr auto IP_inv = make_permutation<8, 8>({
                4, 1, 3, 5, 7, 2, 8, 6
            });

            /**
             * Υλοποίηση του switch με μετάθεση.
             */
            static constexpr auto SW = make_permutation<8, 8>({
                5, 6, 7, 8, 1, 2, 3, 4
            });

            f fK1;
            f fK2;
            bitset<10> _key;
    };

} // namespace sdes


#endif //SDES_CALCULATOR_H
